import { Component, OnInit } from '@angular/core';

//Initializing the map: defined center point and zoom level.

declare let L;
declare let tomtom: any;

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.css']
})

export class MapComponent implements OnInit {
  ngOnInit() {

    //Initializing the map: defined center point and zoom level.
    // we need to take Jeeran API key from REEM 
    const map = tomtom.L.map('map', {
      key: '7VIYwLjMnkrDKkN21Dpw5nCPUQPLBACO',
      basePath: '/assets/sdk',
      center: [37.769167, -122.478468],
      zoom: 15,
      source: 'vector'
    });

    //Displaying the marker

    var marker = tomtom.L.marker([37.768454, -122.492544]).addTo(map);
    marker.bindPopup('my marker');
    tomtom.routing()
      .locations('37.7683909618184,-122.51089453697205:37.769167,-122.478468')
      .go().then(function (routeJson) {
        var route = tomtom.L.geoJson(routeJson, {
          style: { color: '#00d7ff', opacity: 0.6, weight: 6 }
        }).addTo(map);
        map.fitBounds(route.getBounds(), { padding: [5, 5] });
      });

    tomtom.L.marker([37.769167, -122.478468]).addTo(map);
    tomtom.L.marker([37.768014, -122.510601]).addTo(map);

  }

}
