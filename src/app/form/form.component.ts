import { Component, OnInit } from '@angular/core';
// USING:: The FormBuilder service to provide convenient methods for generating controls, instead of crating forms manually ...
import { FormBuilder } from '@angular/forms';
//USING:: To validate against and return an error object or a null value based on the validation check ...
import { Validators } from '@angular/forms';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent implements OnInit {

  ngOnInit() {
  }


  profileForm = this.fb.group({
    firstName: ['', Validators.required],
    lastName: ['', Validators.required],
    email: ['', Validators.required],
    phoneNumber: ['', Validators.required],
    address: this.fb.group({
      street: [''],
      city: ['', Validators.required],
      zip: ['']
    }),
  });

  constructor(private fb: FormBuilder) { }
  onSubmit() {
    // TODO: Use EventEmitter with form value
    console.warn(this.profileForm.value);
  }

}
