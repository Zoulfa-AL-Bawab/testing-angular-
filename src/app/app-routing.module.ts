import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FormComponent } from './form/form.component';
import { from } from 'rxjs';
import { MapComponent } from './map/map.component';
import { AuthComponent } from './auth/auth.component';

const routes: Routes = [
  { path: 'profile', component: FormComponent },
  { path: 'tomtomMap', component: MapComponent },
  { path: 'auth', component: AuthComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
export const routingComponents = [FormComponent, MapComponent, AuthComponent];